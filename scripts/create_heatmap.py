import sys
import corona_analysis.graph_maker as gm
import matplotlib.pyplot as plt
import pandas as pd

if len(sys.argv) < 3:
    print('Parameters: <max value> <csv file(s)>')
    exit()

for file in sys.argv[2:]:
    df = pd.read_csv(file, index_col=0)
    gm.create_heatmap(sys.argv[1], df)
    gm.save_figure(file, 'protein_particle_heatmap')
    plt.show(block=False)
plt.show()


