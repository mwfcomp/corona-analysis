import argparse
import corona_analysis.analyzer as caa
import pandas as pd


parser = argparse.ArgumentParser(description='Performs protein corona analysis functions')
parser.add_argument('sample_excel_file', type=str,
                    help='path to an excel file with sample data. Each sheet is a sample. '
                         'Each sheet should have the following columns exactly: {} and may also '
                         'contain columns named {}'
                    .format([caa.index_col] + caa.sample_df_required_columns, caa.replicate_col_template))
parser.add_argument('protein_excel_file', type=str,
                    help='path to an excel file with protein information. Should contain one sheet with the'
                         'following columns exactly: {}'.format([caa.index_col] + caa.protein_dict_required_columns))
parser.add_argument('blank_control_sheet_name', type=str,
                    help='name of the sheet that contains the "blank control", i.e. the blood or plasma only')

parser.add_argument('sample_name', type=str,
                    help='name of the sample to produce the heatmap for')
parser.add_argument('--raw_expression_heatmap_cutoff', type=float, default=0.1,
                    help='Max value for raw expression protein heatmap')
parser.add_argument('--enrichment_analysis_heatmap_cutoff', type=float, default=10,
                    help='Max value for enrichment analysis protein heatmap')


if __name__ == '__main__':
    args = parser.parse_args()
    caa.raw_expression_heatmap_cutoff = args.raw_expression_heatmap_cutoff
    caa.enrichment_heatmap_cutoff = args.enrichment_analysis_heatmap_cutoff
    caa.saving_file_template = args.sample_excel_file

    sdfs = pd.read_excel(args.sample_excel_file, index_col=caa.index_col, sheet_name=None)
    pdf = pd.read_excel(args.protein_excel_file, index_col=caa.index_col)
    analyzer = caa.Analyzer(sdfs, pdf, args.blank_control_sheet_name, only_secreted=False)
    analyzer.raw_expression_single_heatmap(sample_name=args.sample_name, onlytop=20)
    analyzer.enrichment_analysis_single_heatmap(sample_name=args.sample_name, onlyincontrol=True, onlytop=20)



