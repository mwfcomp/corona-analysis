import argparse
import corona_analysis.analyzer as caa

import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Performs protein corona analysis functions')
parser.add_argument('sample_excel_file', type=str,
                    help='path to an excel file with sample data. Each sheet is a sample. '
                         'Each sheet should have the following columns exactly: {} and may also '
                         'contain columns named {}'
                    .format([caa.index_col] + caa.sample_df_required_columns, caa.replicate_col_template))
parser.add_argument('protein_excel_file', type=str,
                    help='path to an excel file with protein information. Should contain one sheet with the'
                    'following columns exactly: {}'.format([caa.index_col] + caa.protein_dict_required_columns))
parser.add_argument('blank_control_sheet_name', type=str,
                    help='name of the sheet that contains the "blank control", i.e. the blood or plasma only')
parser.add_argument('-g', '--group', nargs='+', action='append', help='List of sample groups to cluster bar graphs and test for commonalities')

parser.add_argument('--raw_expression_heatmap_cutoff', type=float, default=0.1,
                    help='Max value for raw expression protein heatmap')
parser.add_argument('--enrichment_analysis_heatmap_cutoff', type=float, default=10,
                    help='Max value for enrichment analysis protein heatmap')

parser.add_argument('--isoelectric_point_min', type=float, default=7,
                    help='Min value for isoelectric point bargraph')
parser.add_argument('--isoelectric_point_max', type=float, default=7.6,
                    help='Max value for isoelectric point bargraph')

parser.add_argument('--molecular_weight_min', type=float, default=31000,
                    help='Min value for molecular weight bargraph')
parser.add_argument('--molecular_weight_max', type=float, default=99000,
                    help='Max value for molecular weight bargraph')


parser.add_argument('--num_identified_proteins_abundance_bargraph', type=float, default=10,
                    help='Maximum number of identified proteins to show on identified proteins abundance bargraph')
parser.add_argument('--max_abundance_protein_abundance_bargraph', type=float, default=0.15,
                    help="Max value for abundance in protein abundance bargraph")


if __name__ == '__main__':
    args = parser.parse_args()
    caa.saving_file_template = args.sample_excel_file
    print(args.group)
    caa.raw_expression_heatmap_cutoff = args.raw_expression_heatmap_cutoff
    caa.enrichment_heatmap_cutoff = args.enrichment_analysis_heatmap_cutoff

    analyzer = caa.Analyzer.init_from_file_paths(args.sample_excel_file,
                                                 args.protein_excel_file,
                                                 args.blank_control_sheet_name,
                                                 only_secreted=True)
    if args.group:
        analyzer.groupings = args.group
    # combined = analyzer.proteins_to_samples_dataframe(dfs=analyzer.sample_dfs)

    analyzer.get_number_identified_proteins()


    analyzer.raw_expression_heatmap()
    analyzer.raw_expression_heatmap(onlytop=50)

    analyzer.corona_compare_raw_expression_averages_pearson_correlation_heatmap()

    analyzer.enrichment_analysis_heatmap()
    analyzer.enrichment_analysis_heatmap(onlytop=50, onlyincontrol=True)
    analyzer.corona_compare_enrichment_pearson_correlation_heatmap()
    analyzer.corona_compare_raw_expression_rank_correlation_heatmap()
    analyzer.corona_compare_raw_expression_pearson_correlation_heatmap()

    # analyzer.create_protein_abundance_bar_graph_for_each_sample(args.num_identified_proteins_abundance_bargraph,
    #                                                             args.max_abundance_protein_abundance_bargraph)


    # analyzer.secreted_proteins_piechart()
    combined = analyzer.proteins_to_samples_dataframe(analyzer.sample_dfs)


    # analyzer.grouped_isoelectric_barchart(args.isoelectric_point_min, args.isoelectric_point_max)
    # analyzer.grouped_molecular_weight_barchart(args.molecular_weight_min, args.molecular_weight_max)


    analyzer.test_each_group_for_similarly_expressed_proteins()



    # ddf = analyzer.differential_expression()
    # pass
    # analyzer.sample_summaries()







