import corona_analysis.preprocess_protein_data as preprocess
import corona_analysis.file_utility as fu
import sys
import logging
import argparse

do_protein_df = lambda ifile: preprocess.get_protein_info_df(ifile)

def setup_type2_parsing():
    preprocess.columns_to_sample_names = preprocess.columns_to_sample_names_type2
    preprocess.sample_name_to_repl_columns = preprocess.sample_name_to_repl_columns_type2

def setup_type3_parsing():
    global do_protein_df
    preprocess.columns_to_sample_names = preprocess.columns_to_sample_names_type3
    preprocess.sample_name_to_repl_columns = preprocess.sample_name_to_repl_columns_type3
    preprocess.do_isoform_grouping = False
    do_protein_df = lambda ifile: preprocess.get_protein_info_df(ifile, cut_brackets=False)


parser = argparse.ArgumentParser(description='Highly specific preprocessing of NSAF files from unimelb')
parser.add_argument('file_path_to_xlsx', type=str,
                    help='path to an excel file with NSAF sample data in particular format. Abundance column names '
                         'look something like: "1::Sample 1 R1.sepr::NSAF"')
parser.add_argument('--use_type_2_formatting', action='store_true',
                    help='parses file with different column name structure. Abundance column names look something ' \
                         'like: "1::4_Rep1.sepr::Coverage", where 1_4 is sample name and 1 is replicate')
parser.add_argument('--use_type_3_formatting', action='store_true',
                    help='parses file with different column name structure. Abundance column names look something ' \
                         'like: "Intensity GA1", where GA is sample name and 1 is replicate')
parser.add_argument('--do_protein_lookup', action='store_true',
                    help='looks up proteins from UniProt. Can take a long time.')


logging.basicConfig(level=logging.INFO)
args = parser.parse_args()

preprocess.use_replicate = preprocess.always_use_replicate
if args.use_type_2_formatting:
    setup_type2_parsing()
if args.use_type_3_formatting:
    setup_type3_parsing()

infile = args.file_path_to_xlsx
sdfs = preprocess.split_into_sample_dataframes(args.file_path_to_xlsx)
fu.write_ordered_dict_df_to_excel(fu.modfile(infile, 'samples.xlsx'), sdfs)

if args.do_protein_lookup:
    protein_info_df = do_protein_df(infile)
    fu.write_df_to_excel(fu.modfile(infile, 'protein_data.xlsx'), protein_info_df)