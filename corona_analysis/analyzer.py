import pandas as pd
from typing import Dict
from collections import OrderedDict
import corona_analysis.file_utility as fu
import corona_analysis.graph_maker as graph_maker
from functools import reduce
import numpy as np
import parse
import matplotlib.pyplot as plt

index_col = 'pmid'
abundance_col = 'abundance'
abundance_std_dev_col = 'abundance std dev'
replicate_col_contains = 'replicate'
replicate_col_template = 'replicate {}'
sample_df_required_columns = [abundance_col, abundance_std_dev_col]
protein_name_col = 'proteinName'
location_col = 'location'
protein_dict_required_columns = [protein_name_col, 'MW', 'pI', location_col]
saving_file_template = None  # specify the "original" file (or a folder) if you want to save outputs from many function calls automatically
enrichment_col = 'enrichment'
rank_col = 'rank'

overexpression_cutoff = 4.0
underexpression_cutoff = 1. / overexpression_cutoff
raw_expression_heatmap_cutoff = 0.1
enrichment_heatmap_cutoff = 10

graph_maker.abundance_col = abundance_col
graph_maker.name_col = protein_name_col
figure_dpi=1000

# a decorator to auto-save xlsxs
def save_xlsx_decorator(function_returning_df_or_dfs):
    def wrapper(*args, **kwargs):
        if saving_file_template is None:
            return function_returning_df_or_dfs(*args, **kwargs)
        else:
            if type(args[0]) == Analyzer and args[0].only_secreted:
                append_name = '_only_secreted'
            else:
                append_name = ''

            df_or_dfs = function_returning_df_or_dfs(*args, **kwargs)
            new_file = fu.modfile(saving_file_template, function_returning_df_or_dfs.__name__ + append_name + '.xlsx')
            if type(df_or_dfs) is pd.DataFrame:
                fu.write_df_to_excel(new_file, df_or_dfs)
            else:
                fu.write_ordered_dict_df_to_excel(new_file, df_or_dfs, columns=None)
            return df_or_dfs

    return wrapper


# a decorator to auto-save produced images
#Note: modified name generation only works if the calling function uses kwargs (ie names the args).
def save_image_decorator(function_making_graph):
    def wrapper(*args, **kwargs):
        if saving_file_template is None:
            return function_making_graph(*args, **kwargs)
        else:
            if type(args[0]) == Analyzer and args[0].only_secreted:
                append_name = '_only_secreted'
            else:
                append_name = ''
            function_making_graph(*args, **kwargs)
            end_name = function_making_graph.__name__
            for name, val in kwargs.items():
                if val is not None and val is not False:
                    end_name += '_{}{}'.format(name, val)
            new_file = fu.modfile(saving_file_template, end_name + append_name + '.png')
            plt.savefig(new_file, dpi=figure_dpi, bbox_inches='tight')

    return wrapper


class Analyzer:
    def __init__(self,
                 # actually this should be an OrderedDict but typing doesn't work...
                 sample_dfs: Dict[str, pd.DataFrame],
                 protein_df: pd.DataFrame,
                 blank_control_key: str,
                 groupings: [[str]] = None,
                 only_secreted = False):
        self.only_secreted = only_secreted
        self.protein_df = protein_df
        self.sample_dfs = OrderedDict()
        if only_secreted:
            for key, df in sample_dfs.items():
                jdf = _drop_unsecreted_proteins(df, protein_df)
                self.sample_dfs[key] = jdf
        else:
            self.sample_dfs = sample_dfs
        self.blank_control_key = blank_control_key
        self.verify_data()
        self.blank_control = self.sample_dfs[self.blank_control_key]
        self.sde_dfs = self.enrichment_analysis()
        self.samples_with_protein_info = self.sample_summaries()
        if groupings is None:
            self.groupings = list(self.sample_dfs.keys())

    @staticmethod
    def init_from_file_paths(sample_df_path: str,
                             protein_df_path: str,
                             blank_control_key: str,
                             only_secreted=False):
        sdfs = pd.read_excel(sample_df_path, index_col=index_col, sheet_name=None)
        pdf = pd.read_excel(protein_df_path, index_col=index_col)
        return Analyzer(sdfs, pdf, blank_control_key, only_secreted=only_secreted)

    def verify_data(self):
        if self.blank_control_key not in self.sample_dfs.keys():
            raise ValueError('Control sample not present in supplied dataframe dictionary, make sure one '
                             'dataframe is named {}'.format(self.blank_control_key))

        sample_df = self.sample_dfs[self.blank_control_key]

        if sample_df.index.name != index_col or self.protein_df.index.name != index_col:
            raise ValueError('Missing ID column.\n'
                             'Both sample dataframes and protein dataframe need a {} column'.format(index_col))

        scols = sample_df.columns.values
        pcols = self.protein_df.columns.values
        spresent = [s in scols for s in sample_df_required_columns]
        ppresent = [p in pcols for p in protein_dict_required_columns]

        if not (all(spresent) and all(ppresent)):
            raise ValueError('Incorrect columns.\n'
                             'Sample sheets should have the following: {}\n '
                             'Protein info sheet should have the following: {}'.format(sample_df_required_columns,
                                                                                       protein_dict_required_columns))

    @save_xlsx_decorator
    def sample_summaries(self) -> Dict[str, pd.DataFrame]:  # actually an ordered Dict...
        return OrderedDict([(k, self._get_summary_df(v)) for k, v in self.sample_dfs.items()])

    def _get_summary_df(self, df: pd.DataFrame):
        sdf = df.merge(self.protein_df, left_index=True, right_index=True)
        return sdf

    @save_xlsx_decorator
    def enrichment_analysis(self):
        return OrderedDict([(k, self._get_enrichment_df(v)) for k, v in self.sample_dfs.items()])

    # todo: is it worth considering proteins that appear in blank control but not in sample?
    def _get_enrichment_df(self, df: pd.DataFrame):
        ddf = df.merge(self.blank_control, left_index=True, right_index=True, how='left', suffixes=('', '_ctrl'))
        ddf[enrichment_col] = ddf[abundance_col] / ddf['{}_ctrl'.format(abundance_col)]
        replicate_cols = [c for c in df if replicate_col_contains in c]
        for rcol in replicate_cols:
            ddf[rcol] = ddf[rcol] / ddf['{}_ctrl'.format(abundance_col)]

        ddf = ddf.sort_values(by=enrichment_col, ascending=False, na_position='first')
        ddf[rank_col] = ddf[abundance_col].rank(method='average', ascending=False)
        ddf = ddf[[enrichment_col, rank_col] + replicate_cols]
        return ddf

    def test_each_group_for_similarly_expressed_proteins(self):
        for group in self.groupings:
            # group = [pdo.sample_sheet_name.format(s) for s in group]

            overexpressed = lambda df: df[enrichment_col] >= overexpression_cutoff
            combined = self.get_proteins_which_match_criteria_in_all_groups(self.sde_dfs,
                                                                            overexpressed, group)
            print('Overexpressed proteins > {}x serum for {}:'.format(overexpression_cutoff, group))
            for prot in combined[protein_name_col].values:
                print('     {}'.format(prot))

            underexpressed = lambda df: df[enrichment_col] <= underexpression_cutoff
            combined = self.get_proteins_which_match_criteria_in_all_groups(self.sde_dfs,
                                                                            underexpressed, group)
            print('Underexpressed proteins < {}x serum for {}: '.format(underexpression_cutoff, group))
            for prot in combined[protein_name_col].values:
                print('     {}'.format(prot))

    def get_proteins_which_match_criteria_in_all_groups(self, dfs, criteria_fn, sample_groups):

        samples = [dfs[s] for s in dfs if s in sample_groups]
        samples = [s[criteria_fn(s)] for s in samples]

        combined = reduce(lambda a, b: pd.merge(a, b, how='inner', left_index=True, right_index=True), samples)
        combined = combined.merge(self.protein_df, left_index=True, right_index=True)

        return combined

    def proteins_to_samples_dataframe(self, dfs: Dict[str, pd.DataFrame],
                                      use_only_shared_proteins=False,
                                      value_col=abundance_col,
                                      prefill_na_value=np.NaN):

        new_dfs = []

        def get_renaming_function(sample_name):
            def rename(col):
                if col == value_col:
                    return sample_name
                elif replicate_col_contains in col:
                    rnum = parse.parse(replicate_col_template, col)[0]
                    return '{}.{}'.format(sample_name, rnum)
                else:
                    return col

            return rename

        for sample_name, df in dfs.items():
            new_df = df.fillna(prefill_na_value)
            new_df = new_df.rename(get_renaming_function(sample_name), axis='columns')
            new_dfs.append(new_df)

        if use_only_shared_proteins:
            how = 'inner'
        else:
            how = 'outer'

        combined = reduce(lambda a, b: pd.merge(a, b, how=how, left_index=True, right_index=True), new_dfs)
        all_abundance_cols = list(dfs.keys())
        # combined = combined.set_index(name_col)
        combined['total_abundance'] = combined[all_abundance_cols].median(axis=1)
        combined['max_abundance'] = combined[all_abundance_cols].max(axis=1)
        combined = combined.sort_values(by='max_abundance', ascending=False)
        return combined

    @save_xlsx_decorator
    def get_number_identified_proteins(self):
        ndf = pd.DataFrame()

        for name, df in self.sample_dfs.items():
            sdf = df.copy(deep=True)

            sdf = sdf[sdf > 0].count()
            sdf = sdf[~sdf.index.str.contains('abundance')]
            mean = sdf.mean()
            sdf['std'] = sdf.std()
            sdf['mean'] = mean
            ndf[name] = sdf
        return ndf

    @save_xlsx_decorator
    def raw_expression_combined(self):
        df = self.proteins_to_samples_dataframe(self.sample_dfs)
        expression_cols = list(self.sample_dfs.keys())
        df = df[expression_cols]
        df = df.fillna(0)
        df = self._replace_pmid_with_protein_name(df)
        return df

    @save_xlsx_decorator
    def enrichment_combined(self):
        df = self.proteins_to_samples_dataframe(self.sde_dfs,
                                                value_col=enrichment_col,
                                                prefill_na_value=999)
        expression_cols = list(self.sample_dfs.keys())
        # expression_cols = list([ s for s in self.sample_dfs.keys() if s != self.blank_control_key])
        df = df[expression_cols]
        df = self._replace_pmid_with_protein_name(df)
        return df

    @save_image_decorator
    def raw_expression_heatmap(self, onlytop=None):
        redf = self.raw_expression_combined()
        if onlytop is not None:
            redf = redf.head(onlytop)
        graph_maker.create_heatmap(raw_expression_heatmap_cutoff, redf)

    @save_image_decorator
    def raw_expression_single_heatmap(self, sample_name, onlytop=None):
        redf = self.raw_expression_combined()
        redf = pd.DataFrame(redf[sample_name])
        redf = redf.sort_values(by=sample_name, ascending=False, na_position='first')
        if onlytop is not None:
            redf = redf.head(onlytop)
        graph_maker.create_heatmap(raw_expression_heatmap_cutoff, redf)

    @save_image_decorator
    def enrichment_analysis_heatmap(self, onlytop=None, onlyincontrol=False):
        df = self.enrichment_combined()
        if onlyincontrol:
            df = df[pd.notna(df[self.blank_control_key])]
        if onlytop is not None:
            df = df.head(onlytop)
        df = df.fillna(0)
        graph_maker.create_heatmap(enrichment_heatmap_cutoff, df)

    @save_image_decorator
    def enrichment_analysis_single_heatmap(self, sample_name, onlytop=None, onlyincontrol=False):
        df = self.enrichment_combined()
        if onlyincontrol:
            df = df[pd.notna(df[self.blank_control_key])]
        df = df.fillna(0)
        df = pd.DataFrame(df[sample_name])
        df = df.sort_values(by=sample_name, ascending=False)
        if onlytop is not None:
            df = df.head(onlytop)
        graph_maker.create_heatmap(enrichment_heatmap_cutoff, df)


    def column_is_replicate(self, col_name):
        res = parse.parse('{}.{}', col_name)
        if res is None:
            return False
        else:
            return res[0] in self.sample_dfs.keys()

    @save_image_decorator
    def corona_compare_raw_expression_rank_correlation_heatmap(self):
        df = self.proteins_to_samples_dataframe(self.sample_dfs,
                                                value_col=abundance_col)

        expression_cols = [c for c in df.columns if self.column_is_replicate(c)]
        df = df[expression_cols].fillna(0)
        cor = df.corr(method='kendall')
        graph_maker.create_heatmap(1, cor)

    @save_image_decorator
    def corona_compare_raw_expression_pearson_correlation_heatmap(self):
        df = self.proteins_to_samples_dataframe(self.sample_dfs,
                                                value_col=abundance_col)
        expression_cols = [c for c in df.columns if self.column_is_replicate(c)]
        df = df[expression_cols].fillna(0)
        cor = df.corr(method='pearson')
        graph_maker.create_heatmap(1, cor)

    @save_image_decorator
    def corona_compare_raw_expression_averages_pearson_correlation_heatmap(self):
        df = self.raw_expression_combined()
        cor = df.corr(method='pearson')
        graph_maker.create_heatmap(1, cor)

    @save_image_decorator
    def corona_compare_enrichment_pearson_correlation_heatmap(self):
        df = self.proteins_to_samples_dataframe(self.sde_dfs,
                                                value_col=enrichment_col,
                                                prefill_na_value=999)
        expression_cols = [c for c in df.columns if self.column_is_replicate(c)]
        df = df[expression_cols].fillna(0)
        cor = df.corr(method='pearson')
        graph_maker.create_heatmap(1, cor)

    @save_image_decorator
    def grouped_isoelectric_barchart(self, isoelectric_min, isoelectric_max):
        isoelectric_points = [lambda df: df['pI'] < isoelectric_min,
                              lambda df: (df['pI'] < isoelectric_max) & (df['pI'] > isoelectric_min),
                              lambda df: df['pI'] > isoelectric_max]
        isoelectric_point_labels = ['pI<{}'.format(isoelectric_min),
                                    '{}<pI<{}'.format(isoelectric_min, isoelectric_max),
                                    'pi>{}'.format(isoelectric_max)]
        graph_maker.create_grouped_id_protein_barchart(self.groupings, self.samples_with_protein_info,
                                                       protein_segments=isoelectric_points,
                                                       labels=isoelectric_point_labels,
                                                       scale_to_same_height=True)  # can scale to same height if needed

    @save_image_decorator
    def grouped_molecular_weight_barchart(self, mw_min, mw_max):
        molecular_weights = [lambda df: df['MW'] < mw_min,
                             lambda df: (df['MW'] < mw_max) & (df['MW'] > mw_min),
                             lambda df: df['MW'] > mw_max]
        molecular_weight_labels = ['MW<{}'.format(mw_min),
                                   '{}<MW<{}'.format(mw_min, mw_max),
                                   'MW>{}'.format(mw_max)]

        graph_maker.create_grouped_id_protein_barchart(self.groupings, self.samples_with_protein_info,
                                                       protein_segments=molecular_weights,
                                                       labels=molecular_weight_labels,
                                                       scale_to_same_height=False)

    @save_image_decorator
    def total_identified_proteins_barchart(self):
        graph_maker.create_grouped_id_protein_barchart(self.groupings, self.samples_with_protein_info,
                                                       protein_segments=[lambda df: df['MW'] > 0],
                                                       labels=['protein'],
                                                       legend=False)

    def secreted_proteins_piechart(self):
        for s in self.samples_with_protein_info:
            df = self.samples_with_protein_info[s]
            num_of_secreted_proteins = df[df['location'].str.contains('Secreted')].shape[0]
            total_proteins = df.shape[0]
            num_other_proteins = total_proteins - num_of_secreted_proteins
            graph_maker.create_pie_chart([num_of_secreted_proteins, num_other_proteins], ['Secreted', 'Other'])
            new_file = fu.modfile(saving_file_template, 'secreted_proteins_{}.png'.format(s))
            plt.savefig(new_file, dpi=200)

    def create_protein_abundance_bar_graph_for_each_sample(self, number_of_included_proteins, ymax):
        graph_maker.create_protein_abundance_bar_graphs(self.samples_with_protein_info,
                                                        saving_file_template,
                                                        number_of_included_proteins,
                                                        ymax)

    def _replace_pmid_with_protein_name(self, df):
        jdf = df.merge(self.protein_df, left_index=True, right_index=True, how='left')
        jdf.set_index(protein_name_col, inplace=True)
        to_remove = [pcol for pcol in self.protein_df.columns if pcol in jdf.columns]
        jdf = jdf.drop(to_remove, axis='columns')
        return jdf




def _drop_unsecreted_proteins(df, protein_df):
    jdf = df.merge(protein_df, left_index=True, right_index=True, how='left')
    jdf = jdf[jdf[location_col].str.contains("'Secreted'")]

    to_remove = [pcol for pcol in protein_df.columns if pcol in jdf.columns]
    for col in jdf.columns:
        if col not in to_remove and (col == 'abundance' or 'replicate' in col):
            jdf[col] = jdf[col] / jdf[col].sum()

    jdf = jdf.drop(to_remove, axis='columns')
    return jdf
