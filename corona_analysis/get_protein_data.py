import urllib.request
import urllib.error
from bs4 import BeautifulSoup
import logging

uniprot_base_url = 'http://www.uniprot.org/uniprot/'
unknown_protein_msg = 'Unknown protein ID'
unknown_location_msg = 'Unknown location'


#for looking up multiple things
class ProteinLookup:

    def __init__(self, id):
        self.id = id
        self.soup = self._lookup_soup()

    def _lookup_soup(self):
        logging.info('looking up {}'.format(self.id))
        prot_url = get_uniprot_url(self.id)
        with urllib.request.urlopen(prot_url) as file:
            soup = BeautifulSoup(file, 'lxml')
        if soup.entry is None:
            raise ValueError('Unknown Protein ID {}'.format(self.id))
        return soup

    def get_location_info(self):
        locations = self.soup.entry.find_all('subcellularlocation')
        if len(locations) > 0:
            return [l.location.text for l in locations]
        else:
            return unknown_location_msg


    # def get_function_info(self):



def get_uniprot_url(id):
    return uniprot_base_url + id + '.xml'


if __name__ =='__main__':
    print(ProteinLookup('P04434').get_location_info())
    # print(get_subcellular_info('P12345'))
