import pandas as pd
import numpy as np
import os.path
from collections import OrderedDict
import logging
import scipy.stats as stats
import scipy.spatial as spatial
from parse import parse
from functools import reduce
from .get_protein_data import ProteinLookup

# the operations in this file are specific to the proteomics files we're getting from Univeristy of Melbourne's
# proteiomics facilities. Unlikely to be widely useful, while the "protein data operations" code is
# more widely applicable

primary_index_col = 'pmid'
mw_col = 'MW'
pi_col = 'pI'
name_col = 'proteinName'
abundance_col = 'abundance'
abundance_stdev_col = 'abundance std dev'
differential_expr_col = 'differential_expression'
location_col = 'location'
do_isoform_grouping = True
col_order = [abundance_col, abundance_stdev_col]

excluded_cutoff = 0.85
sample_sheet_name = 'Sample {}'

overexpression_prefill_value = 999
logger = logging.getLogger(__name__)


def always_use_replicate(df, col):
    return True


def use_replicate(df, col):
    total = df[col].shape[0]
    counts = df[col].value_counts()[-1]

    excluded = counts / total
    if (excluded > excluded_cutoff):
        logger.info("Excluding replicate {}! (total proteins ID'd: {}, missing proteins: {}, ratio exceeds cutoff of "
                    "{}".format(col, total, counts, excluded_cutoff))
        return False
    return True


def columns_to_sample_names(cols):
    cols = [c for c in cols if 'R1.sepr' in c and '.sepr::NSAF' in c]
    sample_names = [parse('{}::Sample {}', c)[0] for c in cols]
    return sample_names


def sample_name_to_repl_columns(sample_name, cols):
    repl_cols = [c for c in cols if '.sepr::NSAF' in c]
    repl_cols = [c for c in repl_cols if c.startswith('{}::Sample'.format(sample_name))]
    return repl_cols


def columns_to_sample_names_type2(cols):
    cols = [c for c in cols if 'Rep1.sepr::NSAF' in c]
    parsed = [parse('{}::{}_Rep{}', c) for c in cols]
    sample_names = ['{}_{}'.format(p[0], p[1]) for p in parsed]
    return sample_names


def sample_name_to_repl_columns_type2(sample_name, cols):
    parsed = parse('{}_{}', sample_name)
    s0 = parsed[0]
    s1 = parsed[1]
    repl_cols = [c for c in cols if '.sepr::NSAF' in c]
    repl_cols = [c for c in repl_cols if c.startswith('{}::{}_'.format(s0, s1))]
    return repl_cols

def columns_to_sample_names_type3(cols):
    cols = [c for c in cols if 'Intensity' in c and '1' in c]
    parsed = [parse('Intensity {}1', c) for c in cols]
    sample_names = [p[0] for p in parsed]
    return sample_names


def sample_name_to_repl_columns_type3(sample_name, cols):
    repl_cols = [c for c in cols if 'Intensity {}'.format(sample_name) in c]
    return repl_cols


# returns an ordered dict
def split_into_sample_dataframes(file, sample_includes_only_proteins_in_all_replicates=False):
    df = pd.read_excel(file)
    df = df[~df[primary_index_col].str.contains('contaminant')]  # remove rows with "contaminant"
    cols = df.columns
    sample_names = columns_to_sample_names(cols.values)
    # num_samples = len(cols[cols.str.contains(repl_column_identifier.format(1)) &
    #                        cols.str.contains(nsaf_column_identifier)])  # get number of samples

    df_dict = OrderedDict()

    # create a DataFrame for each sample
    for sample_name in sample_names:
        include = True
        used_repl_columns = []
        used_replicate = 1
        replicate_rename_dict = OrderedDict()
        repl_colummns = sample_name_to_repl_columns(sample_name, cols.values)
        # process each replicate
        for repl_col in repl_colummns:
            # specific criteria for whether a replicate is to be included
            if (use_replicate(df, repl_col)):
                present = df[repl_col] != -1
                include = include & present
                df[repl_col] = df[repl_col].replace(-1, 0)
                used_repl_columns.append(repl_col)
                replicate_rename_dict[repl_col] = 'replicate {}'.format(used_replicate)
                used_replicate += 1

        if sample_includes_only_proteins_in_all_replicates:
            sample_df = df[include]
        else:
            sample_df = df.copy()
        sample_df = sample_df[[name_col, primary_index_col] + used_repl_columns]

        used_repl_columns = pd.Series(used_repl_columns)

        # pmids can be repeated - representing the same protein but differently detected isoforms
        # this sums isoforms into a single abundance value
        if do_isoform_grouping:
            sample_df = regroup_isoforms(sample_df, used_repl_columns)
        else:
            sample_df = sample_df.set_index(primary_index_col)
            # sample_df = sample_df.copy()

        sample_df[abundance_col] = sample_df[used_repl_columns].mean(axis=1)
        sample_df[abundance_stdev_col] = sample_df[used_repl_columns].std(axis=1)
        sample_df = sample_df[sample_df[abundance_col] > 0] #eliminate proteins in which no replicate has any abundance
        # make name column prettier
        sample_df = sample_df.rename(columns=replicate_rename_dict)
        df_dict[sample_sheet_name.format(sample_name)] = sample_df[col_order + list(replicate_rename_dict.values())]

    return df_dict


def regroup_isoforms(sample_df, used_repl_columns):
    grouped = sample_df.groupby(primary_index_col)

    grouping = {name_col: lambda cs: max(cs, key=len)}
    for col in used_repl_columns:
        grouping[col] = 'sum'

    # aggregate groupings
    grouped = grouped.aggregate(grouping)
    return grouped


# creates a "dictionary" of protein info from the supplied file.
def get_protein_info_df(file, cut_brackets=True):
    df = pd.read_excel(file, index_col=primary_index_col)
    df = df[~df.index.duplicated(keep='first')]
    df = df[~df.index.str.contains('contaminant')]
    pdf = pd.DataFrame()
    if cut_brackets:
        pdf[name_col] = df[name_col].apply(lambda s: s[:s.find(" [")])
    else:
        pdf[name_col] = df[name_col]

    for c in mw_col, pi_col:
        if c in df.columns:
            pdf[c] = df[c]
        else:
            pdf[c] = -1

    pdf[location_col] = df.index.map(lookup_and_handle_if_missing)
    return pdf

def lookup_and_handle_if_missing(pmid):
    try:
        lookup = ProteinLookup(pmid)
        return lookup.get_location_info()
    except:
        logging.error("***Couldn't lookup PMID {}***".format(pmid))

