from os.path import splitext

import matplotlib.pyplot as plt
import pandas as pd

#need to initialize these before doing anything
abundance_col=None
name_col = None

bar_width = 0.25
between_bar_width = 0.05
between_group_width = .25


def save_figure(source_file, ending):
    filename = '{}_{}.png'.format(splitext(source_file)[0], ending)
    plt.savefig(filename)


def create_grouped_id_protein_barchart(groupings, df_dict, protein_segments=None, labels=None,
                                       scale_to_same_height=False, color=None, legend=True):
    last_bar_loc = 0
    xticks = []
    xtick_labels = []
    first = True
    plt.figure()
    for group in groupings:
        for sample in group:
            df = df_dict[sample]
            plt.gca().set_prop_cycle(None)
            if protein_segments is None:
                id_prots = df.shape[0]
                plt.bar(last_bar_loc, id_prots, width=bar_width)
            else:
                last_id_prot = 0
                scaling = 1
                if scale_to_same_height:
                    scaling = df.shape[0]
                for pn, segment in enumerate(protein_segments):
                    id_prots = df[segment(df)].shape[0] / scaling
                    if first:
                        plt.bar(last_bar_loc, id_prots, width=bar_width, bottom=last_id_prot, label=labels[pn], color=color)
                    else:
                        plt.bar(last_bar_loc, id_prots, width=bar_width, bottom=last_id_prot, color=color)
                    last_id_prot += id_prots
                    pass
            xticks.append(last_bar_loc)
            xtick_labels.append(sample)
            last_bar_loc += bar_width + between_bar_width
            first = False
        last_bar_loc += between_group_width
    if(legend):
        plt.legend()
    plt.xticks(xticks, xtick_labels, rotation='vertical')
    plt.ylabel('Number of identified proteins')


def create_protein_abundance_bar_graphs(df_dict, file, num_proteins=None, ymax=0.0):
    for s in df_dict:
        df = df_dict[s]
        plt.figure()
        sorted = df.sort_values(by=abundance_col, ascending=False)
        if num_proteins is None:
            to_show = sorted
        else:
            to_show = sorted.head(num_proteins)
        xs = range(to_show.shape[0])

        plt.bar(xs, to_show[abundance_col][:].values, width=bar_width)
        plt.xticks(xs, to_show[name_col], rotation=45, ha='right')
        plt.ylabel('Protein abundance')
        if ymax != 0:
            plt.ylim(0, ymax)
            yticks = [0, ymax/2, ymax]
            plt.yticks(yticks, yticks)

        plt.title('{} identified proteins by abundance'.format(s))
        plt.tight_layout()
        plt.savefig('{}_abundance_{}.png'.format(splitext(file)[0], s))


def create_heatmap(max_val, df):
    vmin = 0
    vmax = float(max_val)
    mid = (vmax + vmin) / 2

    plt.figure(figsize=(8.3, .14*df.shape[0]))
    cax = plt.imshow(df, cmap=plt.cm.get_cmap('magma'), aspect='equal', vmin=vmin, vmax=vmax)
    cbar = plt.colorbar(cax, ticks=[vmin, mid, vmax])
    cbar.ax.set_yticklabels([vmin, mid, '>{}'.format(vmax)])
    headers = df.columns.values
    plt.xticks(range(len(headers)), headers, rotation='vertical', fontsize=8)
    plt.yticks(range(len(df.index)), df.index, fontsize=8)
    # plt.tight_layout()

def create_pie_chart(sizes, labels):
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
            shadow=False, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.



