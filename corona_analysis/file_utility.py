import pandas as pd
from os.path import splitext


def modfile(original_file, append_name_and_extension):
    return '{}_{}'.format(splitext(original_file)[0], append_name_and_extension)


def write_df_to_excel(output_file, df, columns=None):
    writer = pd.ExcelWriter(output_file)
    if columns is None:
        df.to_excel(excel_writer=writer)
    else:
        df.to_excel(excel_writer=writer, columns=columns)
    writer.save()


def write_ordered_dict_df_to_excel(output_file, df_dict, columns=None):
    writer = pd.ExcelWriter(output_file)
    for s in df_dict:
        df = df_dict[s]
        if columns is None:
            df.to_excel(excel_writer=writer, sheet_name=s)
        else:
            df.to_excel(excel_writer=writer, sheet_name=s, columns=columns)
    writer.save()
